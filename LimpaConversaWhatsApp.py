#!/usr/bin/python3
#coding: utf-8
import re as regex
import string
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import matplotlib.pyplot as plt
from unicodedata import normalize

fileName = 'WhatsAppPos_2019-04-08.txt'
newFileName = 'Clean'+fileName
regexChatTag = '\d\d/\d\d/\d\d \d\d:\d\d - .*: '
regexChatEntry = '\d\d/\d\d/\d\d \d\d:\d\d - ‎\+\d\d \d\d \d{4,5}-\d{4} entrou usando o link de convite deste grupo'
regexFileSent = '<Arquivo de mídia oculto>'
regexChatExit = '\d\d/\d\d/\d\d \d\d:\d\d - .* saiu'
ignoreList = ["nao", "para", "que", "voce", "uma", "tem", "pra", "sim", "faz", "essa", "como", "ele", "ela", "eles", "elas", "por", "era", "isso", "mas"]

regexes = [regexChatTag, regexChatEntry, regexChatExit, regexChatExit]

def removeRegexes(text, regexes):
    auxText = text
    for i in regexes :
        auxText = regex.sub(i, '', auxText)
    return auxText

def prepareText(text, regexes, ignoredWords):
    text = removeRegexes(text, regexes)
    text = normalize('NFKD', text).encode('ASCII', 'ignore').decode('ASCII')
    printable = set(string.ascii_letters)
    textWords = []
    for i in text.split():
        j = ''.join(filter(lambda x: x in printable, i))
        j = j.lower()
        if (len(j) > 2) and (j not in ignoreList) :
            textWords.append(j)
    return ' '.join(textWords)

def saveCleanFile(fileName, text):
    cleanFile = open(fileName, 'w')
    cleanFile.write(text)
    cleanFile.close()

def createWordCloud(text):
    wordcloud = WordCloud().generate(text)
    # Display the generated image:
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis("off")
    plt.show()


file = open("WhatsAppPos_2019-03-26.txt", 'r') 
chat = prepareText(file.read(), regexes, ignoreList)
saveCleanFile(newFileName, chat)
createWordCloud(chat)